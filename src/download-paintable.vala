public class DownloadPaintable.DownloadIcon : Object, Gdk.Paintable, Gtk.SymbolicPaintable {
    public Gtk.Widget widget { get; construct; }
    public double progress { get; set; default = 0; }

    private Gtk.IconPaintable arrow_paintable;
    private Gtk.IconPaintable check_paintable;

    private Adw.TimedAnimation? done_animation;
    private double check_progress;

    public DownloadIcon (Gtk.Widget widget) {
        Object (widget: widget);

        notify["progress"].connect (invalidate_contents);

        widget.notify["scale-factor"].connect (() => {
            cache_icons ();
            invalidate_size ();
        });

        cache_icons ();
    }

    private void cache_icons () {
        var display = Gdk.Display.get_default ();
        var icon_theme = Gtk.IconTheme.get_for_display (display);
        arrow_paintable = icon_theme.lookup_icon ("download-arrow-symbolic", null, 16, widget.scale_factor, LTR, FORCE_SYMBOLIC);
        check_paintable = icon_theme.lookup_icon ("download-check-symbolic", null, 16, widget.scale_factor, LTR, FORCE_SYMBOLIC);
    }

    public override int get_intrinsic_width () {
        return 16 * widget.scale_factor;
    }

    public override int get_intrinsic_height () {
        return 16 * widget.scale_factor;
    }

    public void snapshot (Gdk.Snapshot snapshot, double width, double height) {
        snapshot_symbolic (snapshot, width, height, {
            { 0.7450980392156863f, 0.7450980392156863f,  0.7450980392156863f,   1.0f },
            { 0.796887159533074f,  0,                    0,                     1.0f },
            { 0.9570458533607996f, 0.47266346227206835f, 0.2421911955443656f,   1.0f },
            { 0.3046921492332342f, 0.6015716792553597f,  0.023437857633325704f, 1.0f }
        });
    }

    public void snapshot_symbolic (Gdk.Snapshot gdk_snapshot, double width, double height, Gdk.RGBA[] colors) {
        var snapshot = gdk_snapshot as Gtk.Snapshot;

        if (check_progress < 1) {
            snapshot.save ();
            snapshot.translate ({ (float) width / 2.0f, (float) height / 2.0f });
            snapshot.scale (1.0f - (float) check_progress, 1.0f - (float) check_progress);
            snapshot.translate ({ (float) width / -2.0f, (float) height / -2.0f });
            arrow_paintable.snapshot_symbolic (gdk_snapshot, width, height, colors);
            snapshot.restore ();
        }

        if (check_progress > 0) {
            snapshot.save ();
            snapshot.translate ({ (float) width / 2.0f, (float) height / 2.0f });
            snapshot.scale ((float) check_progress, (float) check_progress);
            snapshot.translate ({ (float) width / -2.0f, (float) height / -2.0f });
            check_paintable.snapshot_symbolic (gdk_snapshot, width, height, colors);
            snapshot.restore ();
        }

        var cr = snapshot.append_cairo ({{ -2, -2 }, { (float) width + 4, (float) height + 4 }});

        cr.translate (width / 2, height / 2);
        var end = (progress * Math.PI * 2) -Math.PI / 2;

        Gdk.cairo_set_source_rgba (cr, colors[0]);
        cr.arc (0, 0, width / 2 + 1, -Math.PI / 2, end);
        cr.stroke ();

        colors[0].alpha *= 0.25f;
        Gdk.cairo_set_source_rgba (cr, colors[0]);
        cr.arc (0, 0, width / 2 + 1, end, 3 * Math.PI / 2);
        cr.stroke ();
    }

    public void animate_done () {
        if (done_animation != null)
            return;
    
        done_animation = new Adw.TimedAnimation (widget, 0, 1, 500, new Adw.CallbackAnimationTarget (value => {
            check_progress = value;
            invalidate_contents ();
        }));
        done_animation.done.connect (() => {
            if (check_progress > 0.5) {
                done_animation.value_from = 1;
                done_animation.value_to = 0;
                
                int delay = Adw.get_enable_animations (widget) ? 500 : 1000;
                Timeout.add (delay, () => {
                    done_animation.play ();

                    return Source.REMOVE;
                });
            } else {
                done_animation = null;
            }
        });
        done_animation.easing = EASE_IN_OUT_CUBIC;
        done_animation.play ();
    }
}
