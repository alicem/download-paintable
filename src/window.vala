/* window.vala
 *
 * Copyright 2022 Alexander
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

[GtkTemplate (ui = "/org/example/App/window.ui")]
public class DownloadPaintable.Window : Gtk.ApplicationWindow {
    [GtkChild]
    private unowned Gtk.Revealer revealer;
    [GtkChild]
    private unowned Gtk.Image image;
    [GtkChild]
    private unowned Gtk.Popover popover;

    private DownloadIcon icon;
    private Adw.TimedAnimation animation;

    public Window (Gtk.Application app) {
        Object (application: app);
    }

    construct {
        icon = new DownloadIcon (image);

        image.paintable = icon;

        animation = new Adw.TimedAnimation (this, 0, 1, 2000, new Adw.CallbackAnimationTarget (value => {
            icon.progress = value;
        }));
        animation.done.connect (() => {
            if (!popover.visible)
                image.add_css_class ("accent");

            icon.animate_done ();
        });
        animation.easing = LINEAR;
    }

    static construct {
        install_action ("animation.play", null, widget => {
            var self = widget as Window;
            
            self.revealer.reveal_child = true;
            self.image.remove_css_class ("accent");
            self.animation.play ();
        });
        install_action ("download.clear", null, widget => {
            var self = widget as Window;
            
            self.popover.popdown ();
            self.revealer.reveal_child = false;
            self.animation.reset ();
        });
    }

    [GtkCallback]
    private void popdown_cb () {
        image.remove_css_class ("accent");
    }
}
